//
//  ARLabel.m
//  AntRobot
//
//  Created by Pierre-Michel Villa on 27/08/2014.
//  Copyright (c) 2014 Ant Robot. All rights reserved.
//

#import "ARLabel.h"

@implementation ARLabel

-(void)setText:(NSString *)text
{
    [super setText:NSLocalizedString(text, nil)];
}

@end
