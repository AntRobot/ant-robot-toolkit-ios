//
//  ARLabel.h
//  AntRobot
//
//  Created by Pierre-Michel Villa on 27/08/2014.
//  Copyright (c) 2014 Ant Robot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARLabel : UILabel

@end
