//
//  UIView+AntRobot.h
//  AntRobot
//
//  Created by Pierre-Michel Villa on 21/09/2014.
//  Copyright (c) 2014 Ant Robot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (AntRobot)

-(void)disappearAnimated:(BOOL)animated;
-(void)appearAnimated:(BOOL)animated;


@end
