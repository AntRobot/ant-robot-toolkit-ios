//
//  UIView+AntRobot.m
//  AntRobot
//
//  Created by Pierre-Michel Villa on 21/09/2014.
//  Copyright (c) 2014 Ant Robot. All rights reserved.
//

#import "UIView+AntRobot.h"

@implementation UIView (AntRobot)

-(void)disappearAnimated:(BOOL)animated
{
    if (animated) {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.alpha = 0.0f;
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                         }];

    }else
    {
        self.hidden = YES;
    }
}

-(void)appearAnimated:(BOOL)animated
{
    self.hidden = NO;
    
    if (animated) {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.alpha = 1.0f;
                         }
                         completion:^(BOOL finished){
                             
                         }];
        
    }
}

@end
